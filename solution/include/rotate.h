#ifndef ROTATEH
#define ROTATEH
#include "image.h"

void rotate0(struct image* in, struct image* out);

void rotate90(struct image* in, struct image* out);

void rotate180(struct image* in, struct image* out);

void rotate270(struct image* in, struct image* out);
#endif
