#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"
#include "image.h"
#include "rotate.h"

void usage(void) {
    fprintf(stderr,
            "Usage: ./image-transformer <source-image> <transformed-image> <angle>\n");
}

int main( int argc, char** argv ) {
    if (argc != 4)
        usage();

    FILE *f1 = fopen(argv[1], "rb");
    if (!f1) {
        fprintf(stderr, "Bad first input file\n");
        return EXIT_FAILURE;
    }
    FILE *f2 = fopen(argv[2], "wb");
    if (!f2) {
        fclose(f1);
        fprintf(stderr, "Bad second input file\n");
        return EXIT_FAILURE;
    }

    struct image img;
    const enum read_status r_status = from_bmp(f1, &img);
    if (r_status != READ_OK) {
        fprintf(stderr, "Bad source image\n");
        return EXIT_FAILURE;
    }

    struct image rotated;
    rotated.data = malloc(sizeof(struct pixel) * img.height * img.width);
    if (rotated.data == NULL){
        free(rotated.data);
        return EXIT_FAILURE;
    }
    int angle = atoi(argv[3]);
    if (angle < 0){
        angle+=360;
    }
    switch (angle) {
        case 0:
            rotate0(&img, &rotated);
            break;
        case 90:
            rotate90(&img, &rotated);
            break;
        case 270:
            rotate270(&img, &rotated);
            break;
        case 180:
            rotate180(&img, &rotated);
            break;
        default:
            fprintf(stderr, "angle value must be one of 0, 90, -90, 180, -180, 270, -270");
            free(rotated.data);
            free(img.data);
            return EXIT_FAILURE;
    }

    const enum write_status w_status = to_bmp(f2, &rotated);
    if (w_status != WRITE_OK) {
        fprintf(stderr, "Bad output path\n");
        return EXIT_FAILURE;
    }

    free(img.data);
    free(rotated.data);

    fclose(f1);
    fclose(f2);

    return 0;
}
