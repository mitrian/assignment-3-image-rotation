#include "rotate.h"

void rotate0(struct image* in, struct image* out) {
    out->width = in->width;
    out->height = in->height;
    for (int i = 0; i < out->height; ++i) {
        for (int j = 0; j < out->width; ++j) {
            out->data[i * out->width + j] = in->data[i * out->width + j];
        }
    }
}

void rotate90(struct image *in, struct image *out) {
    out->height = in->width;
    out->width = in->height;
    const uint64_t size = in->width * in->height;
    uint64_t out_idx = 0;
    for (int i = 0; i < out->height; ++i) {
        for (uint64_t j = in->width - i - 1; j < size; j += in->width) {
            out->data[out_idx] = in->data[j];
            ++out_idx;
        }
    }
}

void rotate180(struct image* in, struct image* out) {
    out->width = in->width;
    out->height = in->height;
    for (int i = 0; i < out->height; ++i) {
        for (int j = 0; j < out->width; ++j) {
            out->data[(out->height - i - 1) * out->width + out->width - j - 1] = in->data[i * out->width + j];
        }
    }
}

void rotate270(struct image* in, struct image* out) {
    out->height = in->width;
    out->width = in->height;
    const uint64_t size = in->width * in->height;
    uint64_t in_idx = 0;
    for (int i = 0; i < in->height; ++i) {
        for (uint64_t j = out->width - i - 1; j < size; j += out->width) {
            out->data[j] = in->data[in_idx];
            ++in_idx;
        }
    }
}
