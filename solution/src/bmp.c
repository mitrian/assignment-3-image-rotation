#include <stdbool.h>
#include <stdlib.h>

#include "bmp.h"
#define BFTYPE  ('B' | ('M' << 8))
#define UTIL_PADDING 4
#define BIT_COUNT 24
#define BI_SIZE 40
static long get_padding(uint32_t width) { return (UTIL_PADDING - width % UTIL_PADDING) % UTIL_PADDING; }

static bool header_is_correct(const struct bmp_header *header) {

    if (header->bfType != BFTYPE)
        return false;
    if (header->biBitCount != BIT_COUNT)
        return false;

    return true;
}

enum read_status from_bmp(FILE* f, struct image* img) {
    struct bmp_header header = {0};
    if (fread(&header, sizeof(struct bmp_header), 1, f) != 1){
        return READ_INVALID_HEADER;
    }
    if (!header_is_correct(&header)) {
        return READ_INVALID_HEADER;
    }
    img->height = header.biHeight;
    img->width = header.biWidth;
    img->data = malloc(sizeof(struct pixel) * img->width * img->height);
    if (img->data == NULL){
        free(img->data);
        return READ_FAIL;
    }
    const long padding = get_padding(img->width * 3);
    struct pixel* cur = img->data;
    for (int i = 0; i < img->height; ++i) {
        if (fread(cur, img->width * 3, 1, f)!= 1){
            return READ_INVALID_HEADER;
        }
        fseek(f, padding, SEEK_CUR);
        cur += img->width;
    }

    return READ_OK;
}

enum write_status to_bmp(FILE* f, struct image const* img) {
    struct bmp_header header = {0};
    const size_t padding = get_padding(img->width * 3);
    header.bfType = BFTYPE;
    header.bfileSize = (img->width * 3 + padding) * img->height + sizeof(struct bmp_header);
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = BI_SIZE;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = (1 << 8);
    header.biBitCount = BIT_COUNT;
    header.biCompression = header.biSizeImage = header.biXPelsPerMeter = header.biYPelsPerMeter = header.biClrUsed = header.biClrImportant = 0;
    if (fwrite(&header, sizeof(struct bmp_header), 1, f) !=1){
        return WRITE_ERROR;
    }
    uint8_t padding_var = 0;
    struct pixel* cur = img->data;
    for (int i = 0; i < img->height; ++i) {
        if (fwrite(cur, img->width * 3, 1, f)!=1){
            return WRITE_ERROR;
        }
        for (int j = 0; j < padding; ++j) {
            if (fwrite(&padding_var, 1, 1, f) != 1){
                return WRITE_ERROR;
            }
        }
        cur += img->width;
    }

    return WRITE_OK;
}
